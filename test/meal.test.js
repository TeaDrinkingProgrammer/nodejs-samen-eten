process.env.DB_DATABASE = process.env.DB_DATABASE || "studenthome_testdb";
process.env.NODE_ENV = "testing";
process.env.LOGLEVEL = "error";

const chai = require("chai");
const chaiHttp = require("chai-http");
const server = require("../src/index");
const pool = require("../src/config/database");
const logger = require("../src/config/config").logger;
const key = require("../src/config/config").jwtSecretKey;
const jwt = require("jsonwebtoken");
const assert = require("assert");

chai.should();
chai.use(chaiHttp);
chai.use(require("chai-string"));

logger.debug(`Running tests using database '${process.env.DB_DATABASE}'`);

/**
 * Query om alle tabellen leeg te maken, zodat je iedere testcase met
 * een schone lei begint. Let op, ivm de foreign keys is de volgorde belangrijk.
 *
 * Let ook op dat je in de dbconfig de optie multipleStatements: true toevoegt!
 */
const CLEAR_DB =
    "DELETE IGNORE FROM `studenthome`; DELETE IGNORE FROM `meal`; DELETE IGNORE FROM `participants`; DELETE IGNORE FROM `user`";
/**
 * Query om twee studenthome toe te voegen, Deze is nodig om de maaltijden aan te koppelen. Let op de UserId, die moet matchen
 * met de user die je ook toevoegt.
 */

const CLEAR_MEALS = "DELETE IGNORE FROM `meal`;";
/**
 * Query om twee studenthome toe te voegen, Deze is nodig om de maaltijden aan te koppelen. Let op de UserId, die moet matchen
 * met de user die je ook toevoegt.
 */
const INSERT_STUDENTHOMES =
    "INSERT IGNORE INTO `studenthome` (`ID`, `Name`, `Address`, `House_Nr`, `UserId`, `Postal_Code`, `Telephone`, `City`) VALUES " +
    "(1, 'Avans Lovensdijkstraat', 'Lovensdijkstraat', 61, 1, '4818AJ', '+3112345678', 'Breda');";

/**
 * Query om twee movies toe te voegen. Let op de UserId, die moet matchen
 * met de user die je ook toevoegt.
 */
const INSERT_MEALS =
    "INSERT IGNORE INTO `meal` (`ID`, `Name`, `Description`, `Ingredients`, `Allergies`, `CreatedOn`, `OfferedOn`, `Price`, `UserID`,`StudenthomeID`,`MaxParticipants`) VALUES " +
    "(1, 'Mamma Mia Pizzeria', 'Just like my mamma used to make', 'Dough, Tomato sauce, Cheese', 'This dish contains: Cheese', '2021-05-05 15:00:00', '2021-07-03 17:00:00', 11.04, 1,1,4)," +
    "(2, 'Pommes Frites mit Pommes Frites', 'Wie immer', 'Pommes Frites, Pommes Frites', '-', '2021-05-05 15:00:00', '2021-07-03 17:00:00', 5.0, 1,1,4)," +
    "(6, 'Pommes Frites mit Pommes Frites mit Pommes Frites', 'Wie immer', 'Pommes Frites, Pommes Frites', '-', '2021-05-05 15:00:00', '2021-07-03 17:00:00', 5.0, 2,1,4);";

const INSERT_USER =
    "INSERT IGNORE INTO `user` (`ID`, `First_Name`, `Last_Name`, `Email`, `Student_Number`, `Password` ) VALUES" +
    '(1, "first", "last", "name@server.nl","1234567", "Anyoldpassword1!"),' +
    '(2, "Jan", "Janssen", "jan@janssen.nl","1234567", "Anyoldpassword1!");';

describe("Manage Meal", function() {
    /**
     * Deze after staat buiten alle describes() en wordt daarom 1 x aangeroepen
     * na uitvoering van de buitenste describe. Merk op dat deze after niet per sé
     * nodig is, omdat de before altijd start met een lege database.
     */
    after((done) => {
        pool.query(CLEAR_DB, (err, rows, fields) => {
            if (err) {
                console.log(`after error: ${err}`);
                done(err);
            } else {
                logger.info("After FINISHED");
                done();
            }
        });
    });

    before((done) => {
        pool.query(INSERT_USER, (err, rows, fields) => {
            if (err) {
                logger.error(`before CLEARING tables: ${err}`);
                done(err);
            } else {
                logger.debug("insert");
                done();
            }
        });
    });
    before((done) => {
        pool.query(INSERT_STUDENTHOMES, (err, rows, fields) => {
            if (err) {
                logger.error(`before CLEARING tables: ${err}`);
                done(err);
            } else {
                logger.debug("insert");
                done();
            }
        });
    });
    before((done) => {
        pool.query(INSERT_MEALS, (err, rows, fields) => {
            if (err) {
                logger.error(`before CLEARING tables: ${err}`);
                done(err);
            } else {
                logger.debug("insert");
                done();
            }
        });
    });
    describe("Participants", function() {
        describe("Meals", function() {
            describe("create, UC-301", function() {
                it("TC-301-1 should return valid error when required value is not present", (done) => {
                    chai
                        .request(server)
                        .post("/api/studenthome/0/meal")
                        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, key))
                        .send({
                            name: "Mamma Mia Pizzeria",
                            //Description missing
                            addedOn: "05/05/2021",
                            eatenOn: "05/07/2021",
                            price: "11,04",
                            allergyInfo: "This dish contains: cheese",
                            ingredients: ["Dough", "Tomato sauce", "Cheese"],
                        })
                        .end((err, res) => {
                            assert.ifError(err);
                            res.should.have.status(400);
                            res.should.be.an("object");

                            res.body.should.be.an("object").that.has.all.keys("error");
                            res.body.error.should.be
                                .a("string")
                                .that.equals("description is not a string or missing.");
                            done();
                        });
                });

                it("TC-301-2 should return valid error when user is not logged in", (done) => {
                    chai
                        .request(server)
                        .post("/api/studenthome/0/meal")
                        .set("authorization", "Bearer lkjasdfjlkadsjf;lk")
                        .send({
                            name: "Mamma Mia Pizzeria 2.0",
                            description: "hello world",
                            addedOn: "05/02/2021",
                            eatenOn: "05/04/2021",
                            price: "11,04",
                            allergyInfo: "This dish contains: cheeses",
                            ingredients: ["Dough", "Tomato sauce", "Cheeses"],
                        })
                        .end((err, res) => {
                            assert.ifError(err);
                            res.should.have.status(401);
                            res.should.be.an("object");

                            res.body.should.be.an("object").that.has.all.keys("error");
                            res.body.error.should.be
                                .a("string")
                                .that.equals(
                                    "The user is not authorised or the token is expired."
                                );
                            done();
                        });
                });

                it("TC-301-3 should return valid response when meal is succesfully added", (done) => {
                    chai
                        .request(server)
                        .post("/api/studenthome/1/meal")
                        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, key))
                        .send({
                            name: "Mushroom Risotto",
                            description: "WHERE'S THE LAMB SAUCE",
                            createdOn: "2021-05-05 12:00:00",
                            offeredOn: "2021-07-04 15:00:00",
                            userId: 1,
                            price: 20,
                            allergyInfo: "This dish contains: Mushrooms",
                            ingredients: ["Rice", "Mushrooms"],
                            maxParticipants: 4,
                        })
                        .end((err, res) => {
                            res.should.have.status(200);
                            res.body.results[0].should.be
                                .an("object")
                                .that.has.all.keys(
                                    "id",
                                    "name",
                                    "description",
                                    "ingredients",
                                    "allergyInfo",
                                    "offeredOn",
                                    "price",
                                    "createdOn",
                                    "maxParticipants",
                                    "userId",
                                    "studenthomeId"
                                );
                            done();
                        });
                });
            });

            describe("update, UC-302", function() {
                after((done) => {
                    logger.info("studenthome tabel clear");
                    pool.query(CLEAR_MEALS, (err, rows, fields) => {
                        if (err) {
                            logger.error(`before CLEAR error: ${err}`);
                            done(err);
                        } else {
                            done();
                        }
                    });
                });

                after((done) => {
                    logger.info("studenthomes inserted");
                    pool.query(INSERT_MEALS, (err, rows, fields) => {
                        if (err) {
                            logger.error(`before CLEAR error: ${err}`);
                            done(err);
                        } else {
                            done();
                        }
                    });
                });

                it("TC-302-1 should return valid error when required value is not present", (done) => {
                    chai
                        .request(server)
                        .put("/api/studenthome/1/meal/1")
                        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, key))
                        .send({
                            name: "Salt and pepper risotto",
                            //description missing
                            createdOn: "2021-05-05 12:00:00",
                            offeredOn: "2021-07-04 15:00:00",
                            userId: 1,
                            price: 15,
                            allergyInfo: "This dish contains: Salt and pepper",
                            ingredients: ["Rice", "Salt", "Pepper"],
                            maxParticipants: 4,
                        })
                        .end((err, res) => {
                            assert.ifError(err);
                            res.should.have.status(400);
                            res.should.be.an("object");

                            res.body.should.be.an("object").that.has.all.keys("error");
                            res.body.error.should.be
                                .a("string")
                                .that.equals("description is not a string or missing.");
                            done();
                        });
                });
                it("TC-302-2 should return valid error when the user is not logged in", (done) => {
                    chai
                        .request(server)
                        .put("/api/studenthome/1/meal/1")
                        .set("authorization", "Bearer lkjasdfjlkadsjf;lk")
                        .send({
                            name: "Salt and pepper risotto",
                            description: "WHERE'S THE RISOTTO",
                            createdOn: "2021-05-05 12:00:00",
                            offeredOn: "2021-07-04 15:00:00",
                            userId: 1,
                            price: 15,
                            allergyInfo: "This dish contains: Salt and pepper",
                            ingredients: ["Rice", "Salt", "Pepper"],
                            maxParticipants: 4,
                        })
                        .end((err, res) => {
                            assert.ifError(err);
                            res.should.have.status(401);
                            res.should.be.an("object");

                            res.body.should.be.an("object").that.has.all.keys("error");
                            res.body.error.should.be
                                .a("string")
                                .that.equals(
                                    "The user is not authorised or the token is expired."
                                );
                            done();
                        });
                });
                it("TC-302-3 should return valid error when the user is not the owner of the meal", (done) => {
                    chai
                        .request(server)
                        .put("/api/studenthome/1/meal/6")
                        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, key))
                        .send({
                            name: "Salt and pepper risotto",
                            description: "WHERE'S THE RISOTTO",
                            createdOn: "2021-05-05 12:00:00",
                            offeredOn: "2021-07-04 15:00:00",
                            userId: 1,
                            price: 15,
                            allergyInfo: "This dish contains: Salt and pepper",
                            ingredients: ["Rice", "Salt", "Pepper"],
                            maxParticipants: 4,
                        })
                        .end((err, res) => {
                            assert.ifError(err);
                            res.should.have.status(401);
                            res.should.be.an("object");

                            res.body.should.be.an("object").that.has.all.keys("error");
                            res.body.error.should.be
                                .a("string")
                                .that.equals("User is not the owner of this meal.");
                            done();
                        });
                });
                it("TC-302-4 should return valid error when the meal does not exist", (done) => {
                    chai
                        .request(server)
                        .put("/api/studenthome/1/meal/10")
                        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, key))
                        .send({
                            name: "Salt and pepper risotto",
                            description: "WHERE'S THE RISOTTO",
                            createdOn: "2021-05-05 12:00:00",
                            offeredOn: "2021-07-04 15:00:00",
                            userId: 1,
                            price: 15,
                            allergyInfo: "This dish contains: Salt and pepper",
                            ingredients: ["Rice", "Salt", "Pepper"],
                            maxParticipants: 4,
                        })
                        .end((err, res) => {
                            assert.ifError(err);
                            res.should.have.status(404);
                            res.should.be.an("object");

                            res.body.should.be.an("object").that.has.all.keys("error");
                            res.body.error.should.be
                                .a("string")
                                .that.equals("There is no meal with the id: 10.");
                            done();
                        });
                });
                it("TC-302-6 should return valid response when studenthome is succesfully updated", (done) => {
                    chai
                        .request(server)
                        .put("/api/studenthome/1/meal/1")
                        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, key))
                        .send({
                            name: "Salt and pepper risotto",
                            description: "WHERE'S THE RISOTTO",
                            createdOn: "2021-05-05 12:00:00",
                            offeredOn: "2021-07-04 15:00:00",
                            userId: 1,
                            price: 15,
                            allergyInfo: "This dish contains: Salt and pepper",
                            ingredients: ["Rice", "Salt", "Pepper"],
                            maxParticipants: 4,
                        })
                        .end((err, res) => {
                            console.log("!-!");
                            res.should.have.status(200);
                            res.should.be.an("object");
                            res.body.should.be
                                .an("object")
                                .that.has.all.keys("message", "results");
                            res.body.message.should.be
                                .a("string")
                                .that.equals("Meal has been successfully updated.");
                            done();

                            res.body.results[0].should.be
                                .an("object")
                                .that.has.all.keys(
                                    "name",
                                    "id",
                                    "description",
                                    "ingredients",
                                    "allergyInfo",
                                    "offeredOn",
                                    "price",
                                    "createdOn",
                                    "studenthomeId",
                                    "maxParticipants",
                                    "userId"
                                );
                        });
                });
            });

            describe("get, UC-303", function() {
                it("TC-303-1 should return items", (done) => {
                    chai
                        .request(server)
                        .get("/api/studenthome/1/meal")
                        .end((err, res) => {
                            res.should.have.status(200);
                            res.body.results.should.be.an("Array");
                            res.body.results[0].should.be
                                .an("object")
                                .that.has.all.keys(
                                    "name",
                                    "id",
                                    "description",
                                    "ingredients",
                                    "allergyInfo",
                                    "offeredOn",
                                    "price",
                                    "createdOn",
                                    "studenthomeId",
                                    "maxParticipants",
                                    "userId"
                                );
                            res.body.results[1].should.be
                                .an("object")
                                .that.has.all.keys(
                                    "name",
                                    "id",
                                    "description",
                                    "ingredients",
                                    "allergyInfo",
                                    "offeredOn",
                                    "price",
                                    "createdOn",
                                    "studenthomeId",
                                    "maxParticipants",
                                    "userId"
                                );
                            done();
                        });
                });
            });
            describe("get details, UC-304", function() {
                it("TC-304-1 should give error when requesting non existent meal ID", (done) => {
                    chai
                        .request(server)
                        .get("/api/studenthome/0/meal/-1")
                        .end((err, res) => {
                            assert.ifError(err);
                            res.should.have.status(404);
                            res.should.be.an("object");
                            res.body.should.be.an("object").that.has.all.keys("error");
                            res.body.error.should.be
                                .a("string")
                                .that.equals("There is no meal with the id: -1.");
                            done();
                        });
                });
                it("TC-304-2 should return valid response when meal is found", (done) => {
                    chai
                        .request(server)
                        .get("/api/studenthome/0/meal/1")
                        .end((err, res) => {
                            res.should.have.status(200);
                            res.should.be.an("object");
                            res.body.results[0].should.be
                                .an("object")
                                .that.has.all.keys(
                                    "name",
                                    "id",
                                    "description",
                                    "ingredients",
                                    "allergyInfo",
                                    "offeredOn",
                                    "price",
                                    "studenthomeId",
                                    "createdOn",
                                    "maxParticipants",
                                    "userId"
                                );
                            done();
                        });
                });
            });
            describe("delete, UC-305", function() {
                it("TC-305-2 should return valid error when user is not logged in", (done) => {
                    chai
                        .request(server)
                        .delete("/api/studenthome/0/meal/1")
                        .set("authorization", "Bearer lkjasdfjlkadsjf;lk")
                        .end((err, res) => {
                            assert.ifError(err);
                            res.should.have.status(401);
                            res.should.be.an("object");

                            res.body.should.be.an("object").that.has.all.keys("error");
                            res.body.error.should.be
                                .a("string")
                                .that.equals(
                                    "The user is not authorised or the token is expired."
                                );
                            done();
                        });
                });

                it("TC-305-2 should return valid error when user is not the owner of the meal", (done) => {
                    chai
                        .request(server)
                        .delete("/api/studenthome/0/meal/1")
                        .set("authorization", "Bearer " + jwt.sign({ id: 2 }, key))
                        .end((err, res) => {
                            assert.ifError(err);
                            res.should.have.status(401);
                            res.should.be.an("object");

                            res.body.should.be.an("object").that.has.all.keys("error");
                            res.body.error.should.be
                                .a("string")
                                .that.equals("User is not the owner of this meal.");
                            done();
                        });
                });
                it("TC-305-4 should return valid error when meal is not found", (done) => {
                    chai
                        .request(server)
                        .delete("/api/studenthome/0/meal/10")
                        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, key))
                        .end((err, res) => {
                            assert.ifError(err);
                            res.should.have.status(404);
                            res.should.be.an("object");

                            res.body.should.be.an("object").that.has.all.keys("error");
                            res.body.error.should.be
                                .a("string")
                                .that.equals("There is no meal with the id: 10.");
                            done();
                        });
                });

                it("TC-305-5 should return valid response when meal is succesfully deleted", (done) => {
                    chai
                        .request(server)
                        .delete("/api/studenthome/0/meal/1")
                        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, key))
                        .end((err, res) => {
                            res.should.have.status(200);
                            res.should.be.an("object");
                            res.body.should.eql({
                                message: "Meal has been successfully deleted.",
                            });
                            done();
                        });
                });
            });
        });
    });
});