process.env.DB_DATABASE = process.env.DB_DATABASE || "studenthome_testdb";
process.env.NODE_ENV = "testing";
process.env.LOGLEVEL = "error";

const chai = require("chai");
const chaiHttp = require("chai-http");
const server = require("../src/index");
const pool = require("../src/config/database");
const logger = require("../src/config/config").logger;
const key = require("../src/config/config").jwtSecretKey;
const jwt = require("jsonwebtoken");
const assert = require("assert");

chai.should();
chai.use(chaiHttp);

logger.debug(`Running tests using database '${process.env.DB_DATABASE}'`);

/**
 * Query om alle tabellen leeg te maken, zodat je iedere testcase met
 * een schone lei begint. Let op, ivm de foreign keys is de volgorde belangrijk.
 *
 * Let ook op dat je in de dbconfig de optie multipleStatements: true toevoegt!
 */
const CLEAR_DB =
    "DELETE IGNORE FROM `studenthome`; DELETE IGNORE FROM `meal`; DELETE IGNORE FROM `participants`; DELETE IGNORE FROM `user`";
const CLEAR_PARTICIPANTS =
    "DELETE IGNORE FROM `meal`; DELETE IGNORE FROM `participants`;";

/**
 * Query om een studenthome toe te voegen, Deze is nodig om de maaltijden aan te koppelen. Let op de UserId, die moet matchen
 * met de user die je ook toevoegt.
 */
const INSERT_STUDENTHOMES =
    "INSERT IGNORE INTO `studenthome` (`ID`, `Name`, `Address`, `House_Nr`, `UserId`, `Postal_Code`, `Telephone`, `City`) VALUES " +
    "(1, 'Avans Lovensdijkstraat', 'Lovensdijkstraat', 61, 1, '4818AJ', '+3112345678', 'Breda');";

/**
 * Query om twee meals toe te voegen. Let op de UserId, die moet matchen
 * met de user die je ook toevoegt.
 */
const INSERT_MEALS =
    "INSERT INTO `meal` (`ID`, `Name`, `Description`, `Ingredients`, `Allergies`, `CreatedOn`, `OfferedOn`, `Price`, `UserID`,`StudenthomeID`,`MaxParticipants`) VALUES " +
    "(1, 'Mamma Mia Pizzeria', 'Just like my mamma used to make', 'Dough, Tomato sauce, Cheese', 'This dish contains: Cheese', '2021-05-05 15:00:00', '2021-07-03 17:00:00', 11.04, 1,1,4)," +
    "(2, 'Pommes Frites mit Pommes Frites', 'Wie immer', 'Pommes Frites, Pommes Frites', '-', '2021-05-05 15:00:00', '2021-07-03 17:00:00', 5.0, 1,1,4)," +
    "(3, 'Pommes Frites mit Pommes Frites mit Pommes Frites', 'Wie immer', 'Pommes Frites, Pommes Frites', '-', '2021-05-05 15:00:00', '2021-07-03 17:00:00', 5.0, 1,1,4)," +
    "(4, 'Pommes Frites mit Pommes Frites mit Pommes Frites mit Pommes Frites', 'Wie immer', 'Pommes Frites, Pommes Frites', '-', '2021-05-05 15:00:00', '2021-07-03 17:00:00', 5.0, 1,1,4);";

const INSERT_PARTICIPANTS =
    "INSERT INTO `participants` (`UserID`, `StudenthomeID`, `MealID`, `SignedUpOn`) VALUES " +
    "(1, 1, 1, '2021-05-17 14:00:00');";

const INSERT_PARTICIPANTS_IGNORE =
    "INSERT IGNORE INTO `participants` (`UserID`, `StudenthomeID`, `MealID`, `SignedUpOn`) VALUES " +
    "(1, 1, 1, '2021-05-17 14:00:00');";

const INSERT_USER =
    "INSERT INTO `user` (`ID`, `First_Name`, `Last_Name`, `Email`, `Student_Number`, `Password` ) VALUES" +
    '(1, "first", "last", "name@server.nl","1234567", "Anyoldpassword1!");';

/**
 * Deze before staat buiten alle describes() en wordt daarom 1 x aangeroepen
 * voorafgaand aan de buitenste describe.
 */
before((done) => {
    pool.query(CLEAR_DB, (err, rows, fields) => {
        if (err) {
            logger.error(`before CLEARING tables: ${err}`);
            done(err);
        } else {
            done();
        }
    });
});

/**
 * Deze after staat buiten alle describes() en wordt daarom 1 x aangeroepen
 * na uitvoering van de buitenste describe. Merk op dat deze after niet per sé
 * nodig is, omdat de before altijd start met een lege database.
 */
after((done) => {
    pool.query(CLEAR_DB, (err, rows, fields) => {
        if (err) {
            console.log(`after error: ${err}`);
            done(err);
        } else {
            logger.info("After FINISHED");
            done();
        }
    });
});

describe("Manage Participants", function() {
    before((done) => {
        pool.query(CLEAR_DB, (err, rows, fields) => {
            if (err) {
                logger.error(`before CLEARING tables: ${err}`);
                done(err);
            } else {
                logger.debug("clear table");
                done();
            }
        });
    });
    describe("Participants", function() {
        before((done) => {
            pool.query(INSERT_USER, (err, rows, fields) => {
                if (err) {
                    logger.error(`before CLEARING tables: ${err}`);
                    done(err);
                } else {
                    logger.debug("insert");
                    done();
                }
            });
        });
        before((done) => {
            pool.query(INSERT_STUDENTHOMES, (err, rows, fields) => {
                if (err) {
                    logger.error(`before CLEARING tables: ${err}`);
                    done(err);
                } else {
                    logger.debug("insert");
                    done();
                }
            });
        });
        before((done) => {
            pool.query(INSERT_MEALS, (err, rows, fields) => {
                if (err) {
                    logger.error(`before CLEARING tables: ${err}`);
                    done(err);
                } else {
                    logger.debug("insert");
                    done();
                }
            });
        });
        before((done) => {
            pool.query(INSERT_PARTICIPANTS, (err, rows, fields) => {
                if (err) {
                    logger.error(`before CLEARING tables: ${err}`);
                    done(err);
                } else {
                    logger.debug("insert");
                    done();
                }
            });
        });

        beforeEach((done) => {
            pool.query(INSERT_PARTICIPANTS_IGNORE, (err, rows, fields) => {
                if (err) {
                    logger.error(`before CLEARING tables: ${err}`);
                    done(err);
                } else {
                    logger.debug("insert");
                    done();
                }
            });
        });

        describe("create, UC-401", function() {
            it("TC-401-1 should return valid error when user is not logged in", (done) => {
                chai
                    .request(server)
                    .post("/api/studenthome/1/meal/10/signup")
                    .set("authorization", "Bearer lkjasdfjlkadsjf;lk")
                    .end((err, res) => {
                        assert.ifError(err);
                        res.should.have.status(401);
                        res.should.be.an("object");

                        res.body.should.be.an("object").that.has.all.keys("error");
                        res.body.error.should.be
                            .a("string")
                            .that.equals(
                                "The user is not authorised or the token is expired."
                            );
                        done();
                    });
            });

            it("TC-401-2 should return valid error when meal does not exist", (done) => {
                pool.query(INSERT_STUDENTHOMES, (error, result) => {
                    pool.query(INSERT_MEALS, (error, result) => {
                        chai
                            .request(server)
                            .post("/api/studenthome/1/meal/10/signup")
                            .set("authorization", "Bearer " + jwt.sign({ id: 1 }, key))
                            .send({
                                signedUpOn: "2021-03-07 11:00:00",
                            })
                            .end((err, res) => {
                                res.should.have.status(404);
                                res.should.be.an("object");

                                res.body.should.be.an("object").that.has.all.keys("error");
                                res.body.error.should.be
                                    .a("string")
                                    .that.equals("There is no meal with the id: 10.");
                                done();
                            });
                    });
                });
            });

            it("TC-401-2 should return valid response when participant is succesfully added", (done) => {
                pool.query(INSERT_STUDENTHOMES, (error, result) => {
                    pool.query(INSERT_MEALS, (error, result) => {
                        chai
                            .request(server)
                            .post("/api/studenthome/1/meal/2/signup")
                            .set("authorization", "Bearer " + jwt.sign({ id: 1 }, key))
                            .send({
                                signedUpOn: "2021-03-07 11:00:00",
                            })
                            .end((err, res) => {
                                res.should.have.status(200);
                                res.should.be.an("object");
                                res.body.should.be
                                    .an("object")
                                    .that.has.all.keys("message", "results");
                                res.body.message.should.be
                                    .a("string")
                                    .that.equals(
                                        "The participant has been added to the database."
                                    );
                                res.body.results[0].should.be
                                    .an("object")
                                    .that.has.all.keys(
                                        "studenthomeId",
                                        "mealId",
                                        "signedUpOn",
                                        "userId"
                                    );
                                done();
                            });
                    });
                });
            });
        });

        describe("delete, UC-402", function() {
            after((done) => {
                pool.query(INSERT_PARTICIPANTS, (err, rows, fields) => {
                    if (err) {
                        logger.error(`after error: ${err}`);
                        done(err);
                    }
                    if (rows) {
                        done();
                    }
                });
            });
            it("TC-402-1 should return valid error when user is not logged in", (done) => {
                chai
                    .request(server)
                    .delete("/api/studenthome/1/meal/1/signoff")
                    .set("authorization", "Bearer lkjasdfjlkadsjf;lk")
                    .end((err, res) => {
                        assert.ifError(err);
                        res.should.have.status(401);
                        res.should.be.an("object");

                        res.body.should.be.an("object").that.has.all.keys("error");
                        res.body.error.should.be
                            .a("string")
                            .that.equals(
                                "The user is not authorised or the token is expired."
                            );
                        done();
                    });
            });

            it("TC-402-2 should return valid error when the meal does not exist", (done) => {
                chai
                    .request(server)
                    .delete("/api/studenthome/1/meal/100/signoff")
                    .set("authorization", "Bearer " + jwt.sign({ id: 1 }, key))
                    .end((err, res) => {
                        assert.ifError(err);
                        res.should.have.status(404);
                        res.should.be.an("object");

                        res.body.should.be.an("object").that.has.all.keys("error");
                        res.body.error.should.be
                            .a("string")
                            .that.equals("There is no meal with the id: 100.");
                        done();
                    });
            });
            it("TC-402-3 should return valid error when the appointment does not exist", (done) => {
                chai
                    .request(server)
                    .delete("/api/studenthome/1/meal/4/signoff")
                    .set("authorization", "Bearer " + jwt.sign({ id: 1 }, key))
                    .end((err, res) => {
                        assert.ifError(err);
                        res.should.have.status(404);
                        res.should.be.an("object");

                        res.body.should.be.an("object").that.has.all.keys("error");
                        res.body.error.should.be
                            .a("string")
                            .that.equals("Item does not exist.");
                        done();
                    });
            });

            it("TC-402-4 should return valid response when meal is succesfully deleted", (done) => {
                chai
                    .request(server)
                    .delete("/api/studenthome/1/meal/1/signoff")
                    .set("authorization", "Bearer " + jwt.sign({ id: 1 }, key))
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.should.be.an("object");
                        res.body.should.eql({
                            message: "Participant has been successfully deleted.",
                        });
                        done();
                    });
            });
        });

        describe("get all, UC-403", function() {
            it("TC-403-1 should return valid error when user is not logged in", (done) => {
                chai
                    .request(server)
                    .get("/api/meal/1/participants")
                    .set("authorization", "Bearer lkjasdfjlkadsjf;lk")
                    .end((err, res) => {
                        assert.ifError(err);
                        res.should.have.status(401);
                        res.should.be.an("object");

                        res.body.should.be.an("object").that.has.all.keys("error");
                        res.body.error.should.be
                            .a("string")
                            .that.equals(
                                "The user is not authorised or the token is expired."
                            );
                        done();
                    });
            });
            it("TC-403-2 should return valid error when the meal does not exist", (done) => {
                chai
                    .request(server)
                    .get("/api/meal/100/participants")
                    .set("authorization", "Bearer " + jwt.sign({ id: 1 }, key))
                    .end((err, res) => {
                        assert.ifError(err);
                        res.should.have.status(404);
                        res.should.be.an("object");

                        res.body.should.be.an("object").that.has.all.keys("error");
                        res.body.error.should.be
                            .a("string")
                            .that.equals("There is no meal with the id: 100.");
                        done();
                    });
            });
            it("TC-403-3 should return items", (done) => {
                pool.query(INSERT_STUDENTHOMES, (error, result) => {
                    pool.query(INSERT_MEALS, (error, result) => {
                        pool.query(INSERT_PARTICIPANTS, (error, result) => {
                            chai
                                .request(server)
                                .get("/api/meal/1/participants")
                                .set("authorization", "Bearer " + jwt.sign({ id: 1 }, key))
                                .end((err, res) => {
                                    res.should.have.status(200);
                                    res.body.should.be
                                        .an("object")
                                        .that.has.all.keys("message", "results");
                                    res.body.message.should.be
                                        .a("string")
                                        .that.equals("Participants successfully retrieved.");
                                    done();
                                });
                        });
                    });
                });
            });
        });
        describe("get details, UC-404", function() {
            it("TC-404-1 should return valid error when user is not logged in", (done) => {
                chai
                    .request(server)
                    .get("/api/meal/1/participants/100")
                    .set("authorization", "Bearer lkjasdfjlkadsjf;lk")
                    .end((err, res) => {
                        assert.ifError(err);
                        res.should.have.status(401);
                        res.should.be.an("object");

                        res.body.should.be.an("object").that.has.all.keys("error");
                        res.body.error.should.be
                            .a("string")
                            .that.equals(
                                "The user is not authorised or the token is expired."
                            );
                        done();
                    });
            });
            it("TC-404-2 should give error when requesting non existent participant ID", (done) => {
                pool.query(INSERT_STUDENTHOMES, (error, result) => {
                    pool.query(INSERT_MEALS, (error, result) => {
                        pool.query(INSERT_PARTICIPANTS, (error, result) => {
                            chai
                                .request(server)
                                .get("/api/meal/1/participants/100")
                                .set("authorization", "Bearer " + jwt.sign({ id: 1 }, key))
                                .end((err, res) => {
                                    assert.ifError(err);
                                    res.should.have.status(404);
                                    res.should.be.an("object");

                                    res.body.should.be.an("object").that.has.all.keys("error");
                                    res.body.error.should.be
                                        .a("string")
                                        .that.equals("There is no participant with the id: 100.");
                                    done();
                                });
                        });
                    });
                });
            });
            it("TC-404-3 should return valid response when participant is found", (done) => {
                pool.query(INSERT_STUDENTHOMES, (error, result) => {
                    pool.query(INSERT_MEALS, (error, result) => {
                        pool.query(INSERT_PARTICIPANTS, (error, result) => {
                            chai
                                .request(server)
                                .get("/api/meal/1/participants/1")
                                .set("authorization", "Bearer " + jwt.sign({ id: 1 }, key))
                                .end((err, res) => {
                                    res.should.have.status(200);
                                    res.should.be.an("object");
                                    res.body.results[0].should.be.an("object");
                                    res.body.should.be
                                        .an("object")
                                        .that.has.all.keys("message", "results");
                                    done();
                                });
                        });
                    });
                });
            });
        });
    });
});