const participantscontroller = require("../controllers/participantscontroller.js");
const authcontroller = require("../controllers/authenticationcontroller");
const express = require("express");
const router = express.Router();

// UC-301 Participant aanmaken
router.post(
    "/studenthome/:homeId/meal/:mealId/signup",
    authcontroller.validateToken,
    participantscontroller.add
);

// UC-303 Lijst van Participants opvragen
router.get(
    "/meal/:mealId/participants",
    authcontroller.validateToken,
    participantscontroller.get
);

// UC-304 Details van Participants opvragen
router.get(
    "/meal/:mealId/participants/:participantId",
    authcontroller.validateToken,
    participantscontroller.getWhereId
);

// UC-305 Participants verwijderen
router.delete(
    "/studenthome/:homeId/meal/:mealId/signoff",
    authcontroller.validateToken,
    participantscontroller.remove
);

module.exports = router;