const mealcontroller = require("../controllers/mealcontroller.js");
const authcontroller = require("../controllers/authenticationcontroller");
const express = require("express");
const router = express.Router();

// UC-301 Maaltijd aanmaken
router.post("/:homeId/meal/", authcontroller.validateToken, mealcontroller.add);

// UC-302 Maaltijd wijzigen
router.put(
    "/:homeId/meal/:mealId",
    authcontroller.validateToken,
    mealcontroller.update
);

// UC-303 Lijst van maaltijden opvragen
router.get("/:homeId/meal/", mealcontroller.get);

// UC-304 Details van maaltijd opvragen
router.get("/:homeId/meal/:mealId", mealcontroller.getWhereId);

// UC-305 Maaltijd verwijderen
router.delete(
    "/:homeId/meal/:mealId",
    authcontroller.validateToken,
    mealcontroller.remove
);

module.exports = router;