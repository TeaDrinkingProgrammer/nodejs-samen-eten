const studenthomecontroller = require("../controllers/studenthomecontroller");
const authcontroller = require("../controllers/authenticationcontroller");
const express = require("express");
const router = express.Router();

//UC-201 Maak studentenhuis
router.post("/", authcontroller.validateToken, studenthomecontroller.add);

//UC-202 Overzicht van studentenhuizen
router.get("/", studenthomecontroller.get);

//UC-203 Details van studentenhuis
router.get("/:homeId", studenthomecontroller.get);

//UC-204 Studentenhuis wijzigen
router.put(
    "/:homeId",
    authcontroller.validateToken,
    studenthomecontroller.update
);

//UC-205 Studentenhuis verwijderen
router.delete(
    "/:homeId",
    authcontroller.validateToken,
    studenthomecontroller.remove
);

module.exports = router;