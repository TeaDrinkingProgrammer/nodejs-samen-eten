const mysql = require("mysql2/promise");
const logger = require("../config/config").logger;
const dbconfig = require("../config/config").dbconfig;
// require('dotenv').config()

class Database {
    constructor() {
            this._connection;
        }
        /**
         * @description connects to the DB.
         */
    connect() {
            logger.info(`Connecting to: ${dbconfig.host}.`);
            this._connection = mysql.createPool(dbconfig);
        }
        /**
         * @description Used to execute queries
         */
    async execute(query, stmts) {
            return new Promise(async(resolve, reject) => {
                let results;
                try {
                    const rawQuery = await this._connection.query(query, stmts);
                    results = rawQuery[0];
                } catch (error) {
                    return reject(error);
                }
                return resolve(results);
            });
        }
        /**
         * @description disconne.cts application from DB
         */
    disconnect() {
        this._connection.end();
    }
}

module.exports = new Database();