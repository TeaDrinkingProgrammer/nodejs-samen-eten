const database = require("./databaseService");
const logger = require("../config/config").logger;
const genericQuery = require("./genericQueries");

class Home {
    mapResults(results) {
        logger.debug("Mapping results.");
        return results.map((item) => {
            return {
                id: item.ID,
                name: item.Name,
                address: item.Address,
                houseNumber: item.House_Nr,
                userId: item.UserID,
                postalCode: item.Postal_Code,
                phoneNumber: item.Telephone,
                city: item.City,
            };
        });
    }
    async create(home) {
        logger.info(`[DB Home] create`);
        let query = {};
        try {
            query = await genericQuery.create(
                "INSERT INTO `studenthome` (`Name`, `Address`, `House_Nr`, `UserID`, `Postal_Code`, `Telephone`, `City`) VALUES (?, ?, ?, ?, ?, ?, ?)", [
                    home.name,
                    home.address,
                    home.houseNumber,
                    home.userId,
                    home.postalCode,
                    home.phoneNumber,
                    home.city,
                ]
            );
        } catch (error) {
            logger.error(error);
            return error;
        }
        logger.debug("Home has been created.");
        return {
            message: `The ${home.name} home has been added to the database with the id: ${query.insertId}.`,
            results: [{
                id: query.insertId,
                ...home,
            }, ],
            status: 200,
        };
    }

    /**
     * @async
     * @description queries DB with a SELECT query with a specific ID
     * @param {number} homeId
     */
    async readWhereId(homeId) {
        logger.info(`[DB Home] readWhereId`);
        let results = {};
        try {
            results = await genericQuery.readWhereSingle(
                "SELECT * FROM studenthome WHERE ID = ?", [homeId],
                "id",
                "home"
            );
        } catch (error) {
            logger.info(error);
            return error;
        }
        logger.debug("Home has been found.");
        return {
            message: "Studenthomes successfully retrieved",
            results: this.mapResults(results),
            status: 200,
        };
    }

    //Does not use GenericQuery because it is too specific
    async readWhereNameAndCity(name, city) {
        logger.info(`[DB Home] readWhereNameAndCity`);
        let sqlQuery = "";
        let queryItems = [];
        let errorMessage = "";

        //Creates query, depending on the input
        if (name && !city) {
            sqlQuery = "SELECT * FROM studenthome WHERE " + "studenthome.Name = ?";
            queryItems = name;
            errorMessage = `No studenthomes where found with the name ${name}.`;
        } else if (!name && city) {
            sqlQuery = "SELECT * FROM studenthome WHERE " + "studenthome.City = ?";
            queryItems = city;
            errorMessage = `No studenthomes where found in ${city}.`;
        } else if (name && city) {
            sqlQuery =
                "SELECT * FROM studenthome WHERE " +
                "studenthome.Name = ? AND " +
                "studenthome.City = ?";
            queryItems = [name, city];
            errorMessage = `No studenthomes where found with the name ${name} which were in ${city}.`;
        }

        logger.debug("Chosen SQLquery =", sqlQuery);

        //Execute query
        let results = {};
        try {
            results = await database.execute(sqlQuery, queryItems);
        } catch (error) {
            logger.info(error);
            return genericQuery.UNKNOWN_SQL_ERROR;
        }

        //If the results length is zero, there are no items who have met the criteria
        if (results.length !== 0) {
            logger.debug("Home has been found.");
            return {
                message: "Studenthomes successfully retrieved.",
                results: this.mapResults(results),
                status: 200,
            };
        } else {
            //Zie TC-202-1
            logger.debug("No home has been found");
            return { message: errorMessage, results: [], status: 404 };
        }
    }
    async remove(homeId, userId) {
        logger.info(`[DB Home] remove`);
        let results = {};
        try {
            results = await genericQuery.remove(
                "DELETE FROM studenthome WHERE ID = ? AND UserID = ?", [homeId, userId]
            );
        } catch (error) {
            logger.debug(error);
            return error;
        }
        logger.trace("Item was deleted");
        return {
            message: `Studenthome has been successfully deleted.`,
            status: 200,
        };
    }
    async update(home, studentHomeId, userId) {
            logger.info(`[DB Home] update`);
            let results = {};
            try {
                results = await genericQuery.update(
                    "UPDATE studenthome SET Name = ?, Address = ?, House_Nr = ?, UserID = ?, Postal_Code = ?, Telephone = ?, City = ? WHERE ID = ? AND UserID = ?", [
                        home.name,
                        home.street,
                        home.houseNumber,
                        home.userId,
                        home.postalcode,
                        home.phoneNumber,
                        home.city,
                        studentHomeId,
                        userId,
                    ]
                );
            } catch (error) {
                logger.debug(error);
                return error;
            }
            logger.trace("item was updated");
            home.id = studentHomeId;
            home.userId = userId;

            return {
                message: `Studenthome has been successfully updated.`,
                results: [home],
                status: 200,
            };
        }
        // UPDATE studenthome SET Name = "YetAnotherNewName", Address = "someotherStreet", House_Nr = 8, UserID = 1, Postal_Code = "1245AB", Telephone = "+3112345678", City = "Antwerpen" WHERE ID = 1 AND UserID = 1

    async readWhereUserIdAndStudenthomeId(userId, homeId) {
        logger.info(`[DB Home] readWhereUserIdAndhomeId`);
        let results = {};
        try {
            results = await genericQuery.readWhereMultiple(
                "SELECT * FROM studenthome WHERE UserID = ? AND ID = ?", [userId, homeId],
                "homes"
            );
        } catch (error) {
            logger.debug({ error: error });
            if (error === genericQuery.UNKNOWN_SQL_ERROR) {
                return { error: error };
            }
            return { userIsOwner: false };
        }
        return { results: results, userIsOwner: true };
    }
}

module.exports = new Home();