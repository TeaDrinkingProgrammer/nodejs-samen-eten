const database = require("./databaseService");
const logger = require("../config/config").logger;
class GenericQueries {
    UNKNOWN_SQL_ERROR = { message: "Unspecified SQL Error", status: 400 };
    async create(query, items) {
        return new Promise(async(resolve, reject) => {
            logger.info(`[Generic Query] create`);
            let results = {};
            try {
                results = await database.execute(query, items);
            } catch (error) {
                if (error.code === "ER_DUP_ENTRY") {
                    logger.error("Entry already exists");
                    return reject({ message: "Entry already exists.", status: 404 });
                } else if (error.code === "ECONNREFUSED") {
                    logger.error("Connection failure: ", error);
                    return reject({ message: "Database connection failed", status: 404 });
                } else {
                    logger.error(error);
                    return reject(this.UNKNOWN_SQL_ERROR);
                }
            }
            return resolve(results);
        });
    }

    async readWhereSingle(query, items, criterium, name) {
        return new Promise(async(resolve, reject) => {
            logger.info(`[Generic Query] readWhereSingle`);
            let results = {};
            try {
                results = await database.execute(query, items);
            } catch (error) {
                if (error.code === "ECONNREFUSED") {
                    logger.error("Connection failure: ", error);
                    return reject({ message: "Database connection failed", status: 404 });
                } else {
                    logger.error(error);
                    return reject(this.UNKNOWN_SQL_ERROR);
                }
            }

            if (results.length !== 0) {
                logger.debug("More than 0 items where found, returning items");
                return resolve(results);
            } else {
                logger.debug("No items where found");
                return reject({
                    message: `There is no ${name} with the ${criterium}: ${items[0]}.`,
                    status: 404,
                });
            }
        });
    }

    async readWhereMultiple(query, items, name) {
        return new Promise(async(resolve, reject) => {
            logger.info(`[Generic Query] readWhereMultiple`);
            let results = {};
            try {
                results = await database.execute(query, items);
            } catch (error) {
                if (error.code === "ECONNREFUSED") {
                    logger.error("Connection failure: ", error);
                    return reject({ message: "Database connection failed", status: 404 });
                } else {
                    logger.error(error);
                    return reject(this.UNKNOWN_SQL_ERROR);
                }
            }

            if (results.length !== 0) {
                return resolve(results);
            } else {
                logger.debug("No items where found");
                return reject({
                    message: `There are no ${name} that met the criteria.`,
                    status: 404,
                });
            }
        });
    }

    async remove(query, items) {
        return new Promise(async(resolve, reject) => {
            logger.info(`[Generic Query] remove`);
            let results = {};
            try {
                results = await database.execute(query, items);
            } catch (error) {
                logger.error(error);
                return reject(this.UNKNOWN_SQL_ERROR);
            }

            if (results.affectedRows === 0) {
                logger.debug("item was NOT deleted");
                return reject({ message: `Item does not exist.`, status: 404 });
            } else {
                logger.debug("item was deleted");
                resolve();
            }
        });
    }

    async update(query, items) {
        return new Promise(async(resolve, reject) => {
            logger.info(`[Generic Query] update`);
            let results = {};
            try {
                results = await database.execute(query, items);
            } catch (error) {
                logger.info(error);
                return reject(this.UNKNOWN_SQL_ERROR);
            }

            if (results.affectedRows === 0) {
                logger.trace("item was NOT updated");
                return reject({ message: `Item does not exist.`, status: 404 });
            } else {
                resolve();
            }
        });
    }
}

module.exports = new GenericQueries();