//imports
const express = require("express");
const app = express();
const logger = require("./config/config").logger;
const database = require("./services/databaseService");

database.connect();
//standard port
const port = process.env.PORT || 3000;

//import of the studenthomes route
const studenthomeroutes = require("./routes/studenthomeroutes.js");
const mealroutes = require("./routes/mealroutes.js");
const participantsroutes = require("./routes/participantsroutes.js");
const authenticationroutes = require("./routes/authenticationroutes.js");

//Lines to enable POST parsing
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use("/api/studenthome/", studenthomeroutes);
app.use("/api/", authenticationroutes);
//TODO hoe doe je dit?
app.use("/api/studenthome/", mealroutes);
app.use("/api/", participantsroutes);

// Catch-all route
app.all("*", (req, res, next) => {
    logger.info("Catch-all endpoint aangeroepen");
    next({ message: "Endpoint '" + req.url + "' does not exist", status: 401 });
});

app.use((packet, req, res, next) => {
    logger.info("Packet handler called");
    logger.debug("Status: " + packet.status);
    logger.debug("Message: " + packet.message);
    if (packet.status === 200) {
        if (packet.results) {
            res
                .status(packet.status)
                .json({ message: packet.message, results: packet.results });
        } else {
            res.status(packet.status).json({ message: packet.message });
        }
    } else if (packet.status) {
        res.status(packet.status).json({ error: packet.message });
    } else {
        res.status(400).json({ error: "An unknown errror occured" });
    }
});

//Starts the listener and thus the server
app.listen(port, () => {
    logger.info(`App listening at http://localhost:${port}`);
});

module.exports = app;