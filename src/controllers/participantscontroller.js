const inputChecks = require("../inputChecks.js");
const assert = require("assert");
const logger = require("../config/config").logger;
const participant = require("../services/participantService");
const meal = require("../services/mealService");

function participantsChecker(body, next) {
    try {
        console.debug(body);
        assert(typeof body.userId === "number", "user is not a number or missing.");
        assert(
            typeof body.studenthomeId === "number",
            "studenthomeId is not a number or missing."
        );
        assert(
            typeof body.mealId === "number",
            "studenthomeId is not a number or missing."
        );
        assert(
            typeof body.signedUpOn === "string",
            "signedUpOn is not a string or missing."
        );
    } catch (err) {
        console.log("Participants data is invalid: ", err.message);
        next({ message: err.message, status: 400 });
        return false;
    }

    function errorCallback(returnMessage) {
        next({ message: returnMessage, status: 400 });
    }

    if (inputChecks.dateCheck(body.signedUpOn, errorCallback)) {
        logger.debug("Data is valid");
        return true;
    } else {
        return false;
    }
}

function participantsRemoveChecker(body, next) {
    try {
        console.debug(body);
        assert(typeof body.userId === "number", "user is not a string or missing.");
        assert(
            typeof body.studenthomeId === "number",
            "studenthomeId is not a string or missing."
        );
        assert(
            typeof body.mealId === "number",
            "studenthomeId is not a string or missing."
        );
    } catch (err) {
        console.log("Participants data is invalid: ", err.message);
        next({ message: err.message, errCode: 400 });
        return false;
    }
    logger.debug("Data is valid");
    return true;
}

function participantsGetChecker(body, next) {
    try {
        console.debug(body);
        assert(typeof body.userId === "number", "user is not a string or missing.");
        assert(
            typeof body.mealId === "number",
            "studenthomeId is not a string or missing."
        );
    } catch (err) {
        console.log("Participants data is invalid: ", err.message);
        next({ message: err.message, errCode: 400 });
        return false;
    }
    logger.debug("Data is valid");
    return true;
}

class ParticipantController {
    async add(req, res, next) {
        logger.info(`[ParticipantController] add`);
        let data = {
            studenthomeId: parseInt(req.params.homeId),
            mealId: parseInt(req.params.mealId),
            signedUpOn: req.body.signedUpOn,
            userId: req.userId,
        };
        if (participantsChecker(data, next)) {
            let mealCheck = await meal.readWhereId(data.mealId);
            if (mealCheck.results) {
                next(await participant.create(data));
            } else {
                next(mealCheck);
            }
        }
    }
    async remove(req, res, next) {
        logger.info(`[ParticipantController] remove`);
        let data = {
            studenthomeId: parseInt(req.params.homeId),
            mealId: parseInt(req.params.mealId),
            userId: req.userId,
        };

        if (participantsRemoveChecker(data, next)) {
            let participantExistenceCheck = await participant.readWhereId(req.userId);
            if (participantExistenceCheck.results) {
                let mealExistenceCheck = await meal.readWhereId(req.params.mealId);
                if (mealExistenceCheck.results) {
                    logger.debug("User is owner.");
                    next(
                        await participant.remove(
                            parseInt(req.userId),
                            parseInt(req.params.mealId)
                        )
                    );
                } else {
                    logger.debug("Meal does not exist (or error).");
                    next(mealExistenceCheck);
                }
            } else {
                logger.debug("Participant does not exist (or error).");
                next(participantExistenceCheck);
            }
        }
    }
    async getWhereId(req, res, next) {
        logger.info(`[ParticipantController] getWhereId`);
        let data = {
            mealId: parseInt(req.params.mealId),
            userId: parseInt(req.params.participantId),
        };
        if (participantsGetChecker(data, next)) {
            //Extra
            let mealCheck = await meal.readWhereId(data.mealId);
            if (mealCheck.results) {
                next(await participant.readWhereId(data.userId));
            } else {
                next(mealCheck);
            }
        }
    }
    async get(req, res, next) {
        logger.info(`[ParticipantController] get`);
        logger.debug(req.query);
        let mealExistenceCheck = await meal.readWhereId(req.params.mealId);
        if (mealExistenceCheck.results) {
            next(await participant.readAllWhereMealId(req.params.mealId));
        } else {
            next(mealExistenceCheck);
        }
    }
}

module.exports = new ParticipantController();