const assert = require("assert");
const logger = require("./config/config").logger;

module.exports = {
    postalCodeCheck(input, errorCallback) {
        try {
            let re = /[1-9][0-9]{3}([A-RT-Z][A-Z]|[S][BCE-RT-Z])/;
            assert(re.test(input), "postalCode is invalid.");
        } catch (err) {
            errorCallback(err.message);
            return false;
        }
        logger.debug("Postal code is valid");
        return true;
    },

    phoneCheck(input, errorCallback) {
        // Check phoneNr valid formats:
        // (123) 456-7890 | (123)456-7890 | 123-456-7890 | 123.456.7890 | 1234567890 | +31636363634 | 075-63546725
        try {
            let re = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
            assert(re.test(input), "phoneNumber is invalid.");
        } catch (err) {
            errorCallback(err.message);
            return false;
        }
        logger.debug("Phone number is valid");
        return true;
    },

    dateCheck(input, errorCallback) {
        // Check date valid formats:
        // MM/DD/YYYY
        try {
            let re =
                /^(19|20)?[0-9]{2}[-](0?[1-9]|[12][0-9]|3[01])[-](0?[1-9]|1[012])[ ]([0-1][0-9]|[2][0-3])[:]([0-5][0-9]|[6][0])[:]([0-5][0-9]|[6][0])$/g;
            assert(re.test(input), "Date is invalid.");
        } catch (err) {
            errorCallback(err.message);
            return false;
        }
        logger.debug("Date is valid");
        return true;
    },
    emailCheck(input, errorCallback) {
        try {
            //https://www.emailregex.com/
            let re =
                /^(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])$/g;
            assert(re.test(input), "Email is invalid.");
        } catch (err) {
            errorCallback(err.message);
            return false;
        }
        logger.debug("Email address is valid");
        return true;
    },
    passwordCheck(input, errorCallback) {
        try {
            //Minimum eight characters, at least one upper case English letter, one lower case English letter, one number and one special character
            let re =
                /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$ %^&*-]).{8,}$/g;
            assert(
                re.test(input),
                "Password is invalid, It should have a minimum of eight characters, at least one upper case English letter, one lower case English letter, one number and one special character."
            );
        } catch (err) {
            errorCallback(err.message);
            return false;
        }
        logger.debug("password is valid");
        return true;
    },

    priceCheck(input) {
        // Price is valid:
        //  10 | 10,00 !10,0
        try {
            let re = /^(\-)?\d+(\,\d{2})?$/im;
            assert(re.test(input), "Price is invalid.");
        } catch (err) {
            errorCallback(err.message);
            return false;
        }
        return true;
    },
};